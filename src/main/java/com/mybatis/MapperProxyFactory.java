package com.mybatis;


import com.mybatis.config.ConfigMypl;
import com.mybatis.exception.NoAnnotation;
import com.mybatis.exception.NoConfigClass;
import com.mybatis.sqlcache.Cache;
import com.mybatis.sqlcache.impl.ICache;
import com.mybatis.sqloperate.Operations;
import com.mybatis.sqloperate.annotate.*;
import com.mybatis.sqloperate.sql.IOperations;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

import java.util.ArrayList;
import java.util.HashMap;


public class MapperProxyFactory{

    private static IOperations operations = new Operations();

    private static HashMap<String,Cache> sessionCache = new HashMap<>();


    private static ConfigMypl configMypl = new ConfigMypl();

    static {
        //初始化缓存
        ArrayList<String> arrayList = configMypl.getAllMapperLocation();
        for (String name : arrayList){
            try {
                Annotation id = null;
                id = Class.forName(name).getAnnotation(Mapper.class);
                if (id != null) {
                    Cache cache = new ICache(name);
                    sessionCache.put(name,cache);
                }else {
                    throw new NoAnnotation(name);
                }
            }  catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoAnnotation e) {
                e.printStackTrace();
            }
        }
    }


    public static  <T> T getMapper(Class<T> mapper) throws NoAnnotation, NoConfigClass {

        Cache cache = sessionCache.get(mapper.getName());
        if (cache == null){
            throw new NoConfigClass(mapper.getName());
        }
        ((Operations) operations).setSecondaryCache(cache);

        Object proxyInstance = Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class[]{mapper}, (proxy, method, args) -> {

            if(method.getAnnotation(Select.class) != null) {
                return operations.select(method,args,method.getAnnotation(Select.class).value());
            }

            if(method.getAnnotation(Insert.class) != null){
                return operations.insert(method,args,method.getAnnotation(Insert.class).value());
            }

            if(method.getAnnotation(Delete.class) != null){
                return operations.delete(method,args,method.getAnnotation(Delete.class).value());
            }

            if(method.getAnnotation(Update.class) != null){
                return operations.update(method,args,method.getAnnotation(Update.class).value());
            }
            throw new NoAnnotation(method.getName());
        });

        return (T) proxyInstance;
    }

}
