package com.mybatis.sqlcache.impl;

import com.mybatis.sqlcache.Cache;

import java.util.HashMap;

public class ICache implements Cache {

    private final String id;

    private HashMap<Object,Object> cache = new HashMap<>();

    public ICache(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        cache.put(key,value);
    }

    @Override
    public void clear() {
        this.cache.clear();
    }

    @Override
    public int getSize() {
        return cache.size();
    }

    @Override
    public Object getObject(Object key) {
        return this.cache.get(key);
    }

    @Override
    public Object removeObject(Object key) {
        return this.cache.remove(key);
    }


}
