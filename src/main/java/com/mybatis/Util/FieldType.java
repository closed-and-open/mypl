package com.mybatis.Util;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FieldType  {

    public static void setField(Field field, ResultSet resultSet,Object o) throws SQLException, IllegalAccessException {

        Class fType = field.getType();
        if (fType == Integer.class){
            field.set(o,resultSet.getInt(field.getName()));

        }else if( fType == String.class){
            field.set(o,resultSet.getString(field.getName()));

        }else if( fType == Long.class){
            field.set(o,resultSet.getLong(field.getName()));

        }else if( fType == Short.class){
            field.set(o,resultSet.getShort(field.getName()));

        }else if( fType == Float.class ){
            field.set(o,resultSet.getFloat(field.getName()));

        }else if( fType == Double.class){
            field.set(o,resultSet.getDouble(field.getName()));

        }else if( fType == Boolean.class){
            field.set(o,resultSet.getBoolean(field.getName()));

        }else {
            field.set(o,resultSet.getObject(field.getName()));
        }
    }
}
