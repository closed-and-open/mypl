package com.mybatis.Util;

import com.mybatis.sqloperate.annotate.Param;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlParam {

    //将多个参数 放入 sql语句
    public static  String sqlChangeParam(Method method,Object[] args,String sql){
        Parameter[] parameter = method.getParameters();
        int i=0;
        //  修改sql里的参数
        for (Parameter p:parameter) {
            String parameName =  p.getAnnotation(Param.class).value();
            sql = sql.replaceAll("#\\{"+parameName+"}","'" + args[i] +"'");
            i++;
        }
        return sql;
    }

    /**
     *
     * @param args
     * @param clazz  插入对象的类型
     * @param sql
     * @return
     */
    //将一个对象放入sql语句中，还只能在插入语句中使用
    public static  String sqlChangeObject(Object[] args,Class clazz,String sql){

        Field[] fields = clazz.getDeclaredFields();

        for (Field f : fields) {
            //获取字段名字
            String filedName = f.getName();
            //获取字段的get方法名字
            String methodName = "get" + filedName.substring(0,1).toUpperCase()+filedName.substring(1);
            Method method1;
            //修改sql语句
            try {
                method1 = clazz.getDeclaredMethod(methodName, new Class[]{});
                if( sql.indexOf("#{"+filedName+"}") != -1) {

                    sql = sql.replaceAll("#\\{"+filedName+"}","'" +  method1.invoke(args[0], null) +"'");
                }
            }catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return sql;
    }
    //插入多个对象
    public static  String sqlChangeListObject(Object[] args,Class clazz,String sql){

        //获取values
        String pattern = "(?i)values?\\s*\\((.*?)\\)";
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(sql);
        String sqlParam;
        if (matcher.find()) {
            sqlParam = matcher.group(1);
            sqlParam = ",("+sqlParam+")";
        } else {
            System.out.println("未找到匹配的字符串");
            return null;
        }

        int len ;
        int isArray ;
        Object[] array = new Object[0];
        ArrayList<Object> arrayList = null;
        //判断类型是 数组 还是 list
        if(args[0].getClass().isArray()){
            len = ((Object[])args[0]).length;
            array = (Object[])args[0];
            isArray = 1;
        }else if (args[0] instanceof List){
            len = ((ArrayList<Object>)args[0]).size();
            arrayList = (ArrayList<Object>)args[0];
            isArray = 0;
        }else {
            return  null;
        }
        //获取插入对象类型的字段
        Field[] fields = clazz.getDeclaredFields();
        int temp = 0;
        for (int i =0; i < len; i++){
            Object o;
            if(isArray == 1) {
                o = array[i];
            }else {
                o = arrayList.get(i);
            }

            if (o == null){
                continue;
            }
            if(temp != 0) {
                sql = sql + sqlParam;
            }
            for (Field f : fields) {
                //获取字段名字
                String filedName = f.getName();
                //获取字段的get方法名字
                String methodName = "get" + filedName.substring(0,1).toUpperCase()+filedName.substring(1);
                Method method1;
                //修改sql语句
                try {
                    method1 = clazz.getDeclaredMethod(methodName, new Class[]{});
                    if( sql.indexOf("#{"+filedName+"}") != -1) {

                        sql = sql.replaceAll("#\\{"+filedName+"}","'" +  method1.invoke(o, null) +"'");
                        temp = 1;
                    }
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return sql+";";
    }

    //获取方法的返回类型
    public static Class methodReturnType(Method method){

        Class resultType;
        Type genericReturnType = method.getGenericReturnType();
        if(genericReturnType instanceof  Class){
            resultType = (Class) genericReturnType;
        }else if(genericReturnType instanceof ParameterizedType){
            Type[] types = ( (ParameterizedType) genericReturnType) .getActualTypeArguments();
            resultType = (Class)  types[0];
        }else {
            resultType = null;
        }
        return resultType;
    }
}
