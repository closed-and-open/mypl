package com.mybatis.Util;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Slf4j
public class DBUtil {

    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>() ;

    private  static String DRIVER ;
    private  static String URL;
    private  static String USERNAME;
    private  static String PASSWORD;

     static  {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("application.properties");
        Properties properties = new Properties();
         try {
             properties.load(inputStream);
             properties.list(System.out);
             URL = properties.getProperty("db.url");
             DRIVER = properties.getProperty("db.driver");
             USERNAME = properties.getProperty("db.username");
             PASSWORD = properties.getProperty("db.password");
             Class.forName(DRIVER) ;
         } catch (IOException e) {
             e.printStackTrace();
         }catch (ClassNotFoundException e) {
             log.error("驱动加载失败" , e);
         }
    }

    public static Connection getConnection(){
        log.info("获取数据库连接 ...");
        try {
            Connection conn = threadLocal.get() ;
            if(conn == null){
                conn = DriverManager.getConnection(URL,USERNAME,PASSWORD) ;
                threadLocal.set(conn);
            }
            return conn ;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.info("获取数据库连接失败 ...",throwables);
        }
        return null;
    }
}
