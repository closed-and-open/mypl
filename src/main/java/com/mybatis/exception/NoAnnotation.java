package com.mybatis.exception;

public class NoAnnotation extends Exception{
    public NoAnnotation() {
        super("这个方法找不到SQL注解");
    }

    public NoAnnotation(String message) {
        super("这个方法找不到SQL注解,method:"+message);
    }
}
