package com.mybatis.exception;

public class NoConfigClass extends Exception{
    public NoConfigClass() {
        super("没有在配置文件中找到这个类");
    }

    public NoConfigClass(String message) {
        super("没有在配置文件中找到这个类："+message);
    }
}