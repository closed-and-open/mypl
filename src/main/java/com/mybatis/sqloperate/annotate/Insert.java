package com.mybatis.sqloperate.annotate;

import com.entity.Students;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Insert {
    String value();
    Class paramType() default Object.class; //实体类类型,为Object时插入对象会出错
    boolean isList() default  false;        //批量插入
}
