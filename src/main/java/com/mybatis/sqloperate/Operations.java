package com.mybatis.sqloperate;

import com.mybatis.Util.DBUtil;
import com.mybatis.Util.FieldType;
import com.mybatis.Util.SqlParam;
import com.mybatis.sqlcache.Cache;
import com.mybatis.sqloperate.annotate.Insert;
import com.mybatis.sqloperate.sql.IOperations;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Slf4j
public class Operations implements IOperations {

     private Connection conn = DBUtil.getConnection();
     private Cache secondaryCache ;
    /**
     *  根据方法的返回值类型 判断 返回的是实体还是 list
     * @param method    执行的方法
     * @param args      执行的方法的参数
     * @param sql       注解上的sql语句
     * @return  二级缓存
     */
    @Override
    public Object select(Method method, Object[] args, String sql) {
        //  将参数放入sql语句中
        sql = SqlParam.sqlChangeParam( method,  args,  sql);

        //从缓存中查询
        if(secondaryCache.getObject(sql) != null){
            return secondaryCache.getObject(sql);
        }

        //获取实体类的类型
        Class resultType = SqlParam.methodReturnType(method);
        //查询返回类型
        int temp = 0;
        Type genericReturnType = method.getGenericReturnType();
        if(genericReturnType instanceof  Class){
            temp = 1;
        }

        PreparedStatement statement ;
        ResultSet resultSet ;
        //返回的结果集
        List<Object> list = new ArrayList<>();
        try {
            //执行sql语句
            statement = conn.prepareStatement(sql);
            resultSet = statement.executeQuery();

            //获取sql结果的列名
            ResultSetMetaData metaData = resultSet.getMetaData();
            List<String> resultNameList = new ArrayList<>();
            for (int j = 0; j < metaData.getColumnCount(); j++) {
                resultNameList.add(metaData.getColumnName(j+1));
            }

            //将结果赋值给list
            while (resultSet.next()){
                Object a = resultType.newInstance();

                for (String colName : resultNameList){
                    Field f = resultType.getDeclaredField(colName);
                    f.setAccessible(true);
                    FieldType.setField(f,resultSet,a);
                }
                list.add(a);
            }
        }catch (IllegalAccessException e) {
            e.printStackTrace();
        }catch (SQLException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        //判断 查询方法返回类型
        Object result = null;
        if ( temp == 0){
            result = list;
        }else {
            result = list.get(0);
        }

        secondaryCache.putObject(sql,result);
        return result;
    }

    /**
     *  sql插入，参数可以是一个对象,基本类型,对象数组，List
     * @param method
     * @param args
     * @param sql
     * @return
     */
    @Override
    public Integer insert(Method method, Object[] args, String sql) {

        if(method.getAnnotation(Insert.class).paramType() == Object.class && method.getAnnotation(Insert.class).isList() == false){
            //如果方法的参数不是一个对象
            sql = SqlParam.sqlChangeParam( method,  args,  sql);
        }else if( method.getAnnotation(Insert.class).isList() == true){
            //如果方法的参数是多个对象
            Class clazz = method.getAnnotation(Insert.class).paramType();
            sql = SqlParam.sqlChangeListObject(args,clazz, sql);
        } else {
            //如果方法的参数是一个对象
            Class clazz = method.getAnnotation(Insert.class).paramType();
            sql = SqlParam.sqlChangeObject(args,clazz, sql);
        }

        //执行sql语句
        PreparedStatement statement;
        int col=0;
        try {
            statement = conn.prepareStatement(sql);
            col = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(col > 0){
            secondaryCache.clear();
        }
        return col;
    }

    @Override
    public Integer delete(Method method, Object[] args, String sql) {
        //  将参数放入sql语句中
        sql = SqlParam.sqlChangeParam( method,  args,  sql);

        PreparedStatement statement;
        int col=0;
        try {
            statement = conn.prepareStatement(sql);
            col = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(col > 0){
            secondaryCache.clear();
        }
        return col;
    }

    @Override
    public Integer update(Method method, Object[] args, String sql) {
        //  将参数放入sql语句中
        sql = SqlParam.sqlChangeParam( method,  args,  sql);

        PreparedStatement statement = null;
        int col=0;
        try {
            statement = conn.prepareStatement(sql);
            col = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(col > 0){
            secondaryCache.clear();
        }
        return col;
    }

    public void setSecondaryCache(Cache secondaryCache) {
        this.secondaryCache = secondaryCache;
    }
}
