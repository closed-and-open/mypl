package com.mybatis.sqloperate.sql;

import java.lang.reflect.Method;

/**
 * 数据库 增删查改 四个操作
 */
public interface IOperations {

    Object select(Method method, Object[] args, String sql);

    Integer insert(Method method, Object[] args,String sql);

    Integer delete(Method method, Object[] args,String sql);

    Integer update(Method method, Object[] args,String sql);


}
