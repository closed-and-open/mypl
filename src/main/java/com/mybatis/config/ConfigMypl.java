package com.mybatis.config;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConfigMypl {

    private ArrayList<String> allMapperLocation = new ArrayList<>() ;

    public ConfigMypl() {
        try {
            //1、创建工厂
            SAXBuilder builder = new SAXBuilder();
            //2、获取文档
            Document document = builder.build(this.getClass().getClassLoader().getResourceAsStream("mypl.xml")) ;
            //3、获取根节点
            Element rootElement = document.getRootElement();
            //获得 <mappers> 节点
            Element e = rootElement.getChild("mappers") ;
            //mappers 节点的所有子节点；就是所有的<mapper>节点
            List<Element> list = e.getChildren() ;
            for(Element ee : list){
                allMapperLocation.add(ee.getAttributeValue("resource")) ;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAllMapperLocation() {
        return allMapperLocation;
    }


}
