package com.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Students {
    private Integer sno;
    private String sname;
    private String ssex;
    private String clas;

    public Students() {
    }
}
