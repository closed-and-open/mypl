package com.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Score {
    private Integer sno;
    private String cno;
    private Double degree;

    public Score() {
    }
}
