package com.mapper;

import com.entity.Score;

import com.mybatis.sqloperate.annotate.Delete;
import com.mybatis.sqloperate.annotate.Mapper;
import com.mybatis.sqloperate.annotate.Param;
import com.mybatis.sqloperate.annotate.Select;

import java.util.List;
@Mapper
public interface ScoreMapper {

    @Select("select * from score")
    public List<Score> getAllScore();

    @Delete("delete FROM score WHERE sno = #{sno}")
    Integer delectByIdScore(@Param("sno") Integer sno);


}
