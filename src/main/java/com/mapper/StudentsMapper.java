package com.mapper;

import com.entity.Students;
import com.mybatis.sqloperate.annotate.*;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface StudentsMapper {

    @Select("select * from students")
    public List<Students> getAllStudents();

    @Select("select * from students where sname = #{sname} and sno = #{sno} ")
    public ArrayList<Students> getStudents(@Param("sname") String sname, @Param("sno") Integer sno);

    @Select("select * from students where  sno = #{sno} ")
    public Students getByIdStudent(@Param("sno") Integer sno);

    @Insert(value = "INSERT INTO students ( sno, sname,ssex,clas )VALUES ( #{sno} , #{sname},#{ssex},#{clas})",
            paramType = Students.class)
    Integer insertStudent(Students students);

    @Insert(value = "INSERT INTO students ( sno, sname,ssex,clas )VALUES ( #{sno} , #{sname},#{ssex},#{clas})",
            paramType = Students.class,
            isList = true)
    Integer insertStudent2(ArrayList<Students> students);

    @Insert(value = "INSERT INTO students ( sno, sname,ssex,clas )VALUES ( #{sno} , #{sname},#{ssex},#{clas})",
            paramType = Students.class,
            isList = true)
    Integer insertStudent3(Students[] students);


    @Insert("INSERT INTO students ( sno, sname,ssex )VALUES ( #{sno} , #{sname}, #{ssex})")
    Integer insertStudent(@Param("sno")Integer sno,@Param("sname") String sname,@Param("ssex") String ssex);


    @Delete("DELETE FROM students WHERE sno = #{sno}")
    Integer delectStudent(@Param("sno") Integer sno);

    @Update("UPDATE students SET ssex = #{ssex},sname = #{sname} WHERE sno = #{sno}")
    Integer updateStudent(@Param("sno") Integer sno,@Param("ssex") String ssex,@Param("sname") String sname);

}
