package com;



import com.entity.Students;
import com.mapper.StudentsMapper;
import com.mybatis.MapperProxyFactory;
import com.mybatis.config.ConfigMypl;
import com.mybatis.exception.NoAnnotation;
import com.mybatis.exception.NoConfigClass;


public class MyApplication {
    public static void main(String[] args) {

        StudentsMapper studentsMapper = null;
        try {
            studentsMapper = MapperProxyFactory.getMapper(StudentsMapper.class);
        } catch (NoAnnotation e) {
            e.printStackTrace();
        } catch (NoConfigClass e) {
            e.printStackTrace();
        }

        System.out.println( studentsMapper.getAllStudents());

        System.out.println( studentsMapper.getAllStudents());
    }
}
