# mypl

#### 介绍
个人实现的一个简单的mybatis

#### 软件架构
软件架构说明
功能
1.0
     只在Mysql上使用过，其他数据库不知道能不能运行。
     sql语句只能写在映射接口的方法上，使用方法与mybatis差不多(参数必须用@Param注解标记占位符)，还不会弄写在xml文件里的。
     insert 可以插入对象、对象数组、对象列表。返回值是一个int。
     delete 与 update 的操作差不多。
     select 返回类型为一个实体类或一个ArrayList对象。
     二级缓存


    


#### 使用说明

1.  在entity包下创建实体类
2.  在mapper包下创建映射接口
3.  在mypl.xml文件里 写好映射接口的地址。在application.properties文件里设置好数据库的连接
3.  `MapperProxyFactory.getMapper(映射接口.class);` 即可获得接口的实体类

#### 个人留言
一级缓存要在每个数据库操作方法里调用吗？我在研究研究。





